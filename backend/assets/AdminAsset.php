<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminAsset extends AssetBundle
{
//    public $sourcePath = '@bower/';
    public $basePath = '@webroot';
    public $baseUrl = '@web';/**/
    public $css = [
        'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css',
		'css/colors.css',		
		'css/site.css',
	];
    public $js = [
		'js/jquery.color.js',
        'js/colorpicker/colors.js',
        'js/colorpicker/jqColorPicker.js',
		'js/moment.js',
		'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js',
		'js/modal.js',
        'js/site.js',
	];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}