<?php

namespace backend\controllers;

use Yii;
use backend\models\Address;
use backend\models\SearchAddress;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;


/**
 * AddressController implements the CRUD actions for Address model.
 */
class AddressController extends Controller
{
    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['post'],
    //             ],
    //         ],
    //     ];
    // }

    /**
     * Lists all Address models.
     * @return mixed
     */
    public function actionIndex($parent_id = 0, $type = 1)
    {
        $searchModel = new SearchAddress();
        $params = Yii::$app->request->queryParams;

		$params['SearchAddress']['parent_id'] = $parent_id;
		
        $dataProvider = $searchModel->search($params);
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$current = SearchAddress::find()->where(['id'=>$parent_id])->one();

        if(empty($type))
            $type = 1;
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'type' => $type,
            'parents' => SearchAddress::parents($parent_id),
            'parent_id' => $parent_id,
            'current' => $current,			
            // 'name' => $name,
        ]);
    }

    /**
     * Displays a single Address model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Address model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($parent_id = 0, $type = 0/**/)
    {
        $temp = SearchAddress::find()->where(['id' => $parent_id])->one();
        $id = $temp['id'];
        $dependency_id = $temp['dependency_id'];
        $name = $temp['name'];
        $type = $temp['type'];

        $model = new Address();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'parent_id' => $id,
                'type' => $type+1,
            ]);
        }
    }

    /**
     * Updates an existing Address model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $temp = SearchAddress::find()->where(['id' => $id])->one();
        $type = $temp['type'];
        $parent_id = $temp['parent_id'];
        $dependency_id = $temp['dependency_id'];
        $name = $temp['name'];


        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'parent_id' => $parent_id,
                'type' => $type,
            ]);
        }
    }

    /**
     * Deletes an existing Address model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(SearchAddress::canDelete($id)){
        $this->findModel($id)->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Address model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Address the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Address::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionList($id = 0/**/)
    {
        $temp = SearchAddress::find()->where(['parent_id' => $id])->all();
        $temp = ArrayHelper::map($temp, 'id', 'name');
        $result =  Html::dropDownList('my-select', NULL, $temp, $options = []);
        return $result;
    }

    public function getFullPath($id = 0 /**/){
        $temp = SearchAddress::parents($id);
        $result = "";
        $result = implode(array_reverse(ArrayHelper::map($temp, 'id', 'name')), " ");
        return $result;

    }
    public function showName($id = 0){
        $result = "";
        $result = SearchAddress::find()->where(['id' => $id])->one()['name'];
        return $result;

    }
}
