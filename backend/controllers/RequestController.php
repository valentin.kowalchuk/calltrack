<?php

namespace backend\controllers;

use app\models\Address;
use app\models\CallRequest;
use backend\models\SearchAddress;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


/**
 * RequestController implements the CRUD actions for CallRequest model.
 */
class RequestController extends Controller
{
    public function init()
    {		
		//$this->bottstrap_css = true;
		Yii::$app->language = 'ru-RU';
	}
	
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['update'],
                        'allow' => true,
						'roles' => ['request_edit']
                    ],
                    [
                        'actions' => ['done'],
                        'allow' => true,
						'roles' => ['request_done']
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
						'roles' => ['request_view']
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
						'roles' => ['request_create']
                    ],
                    [
                        'actions' => ['getaddrinp'],
                        'allow' => true,
						//'roles' => ['request_create']
                    ],
                    [
                        'actions' => ['getaddrinp2'],
                        'allow' => true,
						//'roles' => ['request_create']
                    ],
                    [
                        'actions' => ['view-posible-dependency'],
                        'allow' => true,
						//'roles' => ['request_create']
                    ],
                    [
                        'actions' => ['get-sub-crash'],
                        'allow' => true,
						//'roles' => ['request_create']
                    ],
					
                ],
            ],		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CallRequest models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RequestSearch();
		$queryParams = Yii::$app->request->queryParams;
		if (!array_key_exists('sort', $queryParams))
		{
			return $this->redirect(['index','sort'=>'-id']);
		}

        $dataProvider = $searchModel->search($queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

	public function actionGetSubCrash($id)
    {
		$a_sub_crash = CallRequest::find()->where(['crash_id'=>$id]);
		$dataProvider = new \yii\data\ActiveDataProvider([
			'query' => $a_sub_crash,
		]);


		return $this->renderPartial('sub_crash_tr', [
            'searchModel' => null,
            'dataProvider' => $dataProvider,
			'only_rec' => true,
        ]);
    }

    /**
     * Displays a single CallRequest model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderPartial('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CallRequest model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

	public function actionCreate()
	{
        $model = new \app\models\CallRequest();
		$model->scenario = 'create';
		
		if(Yii::$app->request->post()==array())
		{
			return $this->renderPartial('m_create', [
				'model' => $model,
				'title' => 'Create Call Request',
			]);
		}
		
		// сохранение
        if($model->load(Yii::$app->request->post()) && $model->validate() && $model->save() /*&& $model->contact(Yii::$app->params['adminEmail'])/**/) 
		{
			$responsible_user = ArrayHelper::getValue(Yii::$app->request->post(), 'CallRequest.responsible_user', array());
			if (!is_array($responsible_user)) $responsible_user = [$responsible_user];
			$model->setResponsibleUsers($responsible_user);
			
            $result=[
				'result'=>true,
				'msg'=>'Сохранено'// . print_r(Yii::$app->request->post('CallRequest')['responsible_user'],1)
			];
            return json_encode($result);
        }
		// если сохранение не удалось или не прошла проверка
        else
        {
            $result=[
				'result'=>false,
				'content'=> $this->renderPartial(
					'm_create', [
						'model' => $model,
						'title' => 'Create Call Request',
					]
				),
				'msg'=>'Ошибка'
			];
            return json_encode($result);			
        }
    }

    /**
     * Updates an existing CallRequest model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		$model = $this->findModel($id);
		if(Yii::$app->request->post()==array())
		{
			return $this->renderPartial('m_create', [
				'model' => $model,
				'title' => 'Update Call Request',
			]);
		}
		
		$model->scenario = 'update';
		// сохранение
        if($model->load(Yii::$app->request->post()) && $model->validate() && $model->canEdit() && $model->save()) 
		{
            $result=[
				'result'=>true,
				'msg'=>'Сохранено'// . print_r(Yii::$app->request->post('CallRequest')['responsible_user'],1)
			];
            return json_encode($result);
        }
		// если сохранение не удалось или не прошла проверка
        else
        {
            $result=[
				'result'=>false,
				'content'=> $this->renderPartial(
					'm_create', [
						'model' => $model,
						'title' => 'Update Call Request',
					]
				),
				'msg'=>'Ошибка' //. print_r($rr,1)
			];
            return json_encode($result);
        }
    }

    public function actionViewPosibleDependency($id)
    {
		$a_dependency = CallRequest::getPosibleDependencyAddress($id);
		foreach ($a_dependency as &$rec)
		{
			$rec->text = "<div class = \"col-sm-9\"><div>{$rec->address}</div><div>{$rec->comment}</div></div>";
		}
		unset($rec);
		$a_address_history = [];
		
		$a_address_history = CallRequest::find($id)->where(['address_id'=>$id])->all();
		
		return $this->renderPartial('m_posible_dependency', [
			'a_dependency' => $a_dependency,
			'a_address_history' => $a_address_history,
			'title' => 'Зависимость от аварии',
		]);
	}
	
    public function actionDone($id)
    {
		$model = $this->findModel($id);
		if(Yii::$app->request->post()==array())
		{
			return $this->renderPartial('done', [
				'model' => $this->findModel($id),
			]);
		}
		else
		if($model->canEdit())
		{
			$post = Yii::$app->request->post('CallRequest');
			$model->comment_done = ArrayHelper::getValue(Yii::$app->request->post(), 'CallRequest.comment_done', null);
			$model->comment_done = ArrayHelper::getValue(Yii::$app->request->post(), 'CallRequest.comment_done', null);
			$model->time_performed = ArrayHelper::getValue(Yii::$app->request->post(), 'CallRequest.time_performed', null);
			$responsible_user = ArrayHelper::getValue(Yii::$app->request->post(), 'CallRequest.responsible_user', array());
			if (!is_array($responsible_user))
				$responsible_user = [$responsible_user];
			if (implode($responsible_user,'') == '')
				$responsible_user[] = Yii::$app->user->getId();
			$model->scenario = 'done';
			if($model->validate() && $model->save())
			{
				if(Yii::$app->user->can('operator')){
					$model->setResponsibleUsers($responsible_user);
				}
				else
				{
					$model->setResponsibleUser(Yii::$app->user->getId());
				}
				$result=[
					'result'=>true,
					'content'=> $this->renderPartial(
						'done', ['model' => $model]
					),
					'msg'=>'Выполнено'
				];
				return json_encode($result);
			}
			else
			{
				$result=[
					'result'=>!true,
					'content'=> $this->renderPartial(
						'done', ['model' => $model]
					),
					'msg'=>'Ошибка ' . implode($model->getErrors('closed'), ', ')
				];
				return json_encode($result);		
			}
		}
    }

    /**
     * Deletes an existing CallRequest model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CallRequest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CallRequest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CallRequest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
    public function actionGetaddrinp($id = 0)
    {
		$uu = Yii::$app->request->post();
        $temp = SearchAddress::find()->where(['parent_id' => $id])->orderBy('name')->all();
		array_unshift($temp, ['id'=>0, 'name'=>Yii::t('app', '-select-')]);
        $temp = ArrayHelper::map($temp, 'id', 'name');
        $result =  Html::dropDownList('my-select', NULL, $temp, $options = []); /**/
        return $result;
    }

    public function renderAddrinp($id = 0)
    {
        $temp = SearchAddress::find()->where(['parent_id' => $id])->all();
		array_unshift($temp, ['id'=>0, 'name'=>Yii::t('app', '-select-')]);
        $temp = ArrayHelper::map($temp, 'id', 'name');
		$result =  Html::dropDownList('my-select', NULL, $temp, $options = []);
	}
	
    public function actionGetaddrinp2($id = 0)
    {
        return self::renderAddrinp($id);
    }
}
