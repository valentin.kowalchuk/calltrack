<?php

namespace backend\controllers;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CallRequest;

/**
 * RequestSearch represents the model behind the search form about `app\models\CallRequest`.
 */
class RequestSearch extends CallRequest
{
    /**
     * @inheritdoc
     */

	public $time_add_min;
	public $time_add_max;
	public $time_performed_min;
	public $time_performed_max;
	public $time_expected_start_min;
	public $time_expected_start_max;
	
	public $performed = 1;
	public $responsible_user;	
	
    public function rules()
    {
        return [
            [['id', 'type', 'phone', 'status', 'responsible_user'/**/], 'integer'],
            [['contact'], 'string'],
            [['time_performed', 'address', 'comment'], 'safe'],
            [['time_add'], 'safe'],
			[['time_add_min', 'time_add_max', ], 'safe'],
			[['time_performed_min', 'time_performed_max', ], 'safe'],
			[['time_expected_start_min', 'time_expected_start_max', ], 'safe'],
			[['performed', ], 'safe'],
		];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CallRequest::find();//->orderBy(['id' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => array('pageSize' => 25),
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'call_request.id' => $this->id,
            'call_request.type' => $this->type,
            'call_request.phone' => $this->phone,
            'call_request.contact' => $this->contact,
            'call_request.status' => $this->status,
        ]);
		
		$query->andFilterWhere(['>=', 'call_request.time_add', $this->time_add_min]);
		$query->andFilterWhere(['<=', 'call_request.time_add', $this->time_add_max]);
		
		$query->andFilterWhere(['>=', 'call_request.time_performed', $this->time_performed_min]);
		$query->andFilterWhere(['<=', 'call_request.time_performed', $this->time_performed_max]);

		//attribute_filter
		$query->andFilterWhere(['>=', 'call_request.time_expected_start', $this->time_expected_start_min]);
		$query->andFilterWhere(['<=', 'call_request.time_expected_start', $this->time_expected_start_max]);
        $query->andFilterWhere(['like', 'call_request.address', $this->address])
            ->andFilterWhere(['like', 'call_request.comment', $this->comment]);

		//фильтр по выполнено/не выполнено
		if($this->performed == 1)			
			$query->andWhere(['call_request.time_performed' => null]);
		if($this->performed == 2)			
			$query->andWhere(['not', ['call_request.time_performed' => null]]);
		$query->andWhere(['call_request.crash_id' => null]);

		//фильтр по пользователям
		if(!empty($this->responsible_user))
			$query->innerJoin('responsible','responsible.call_request_id = call_request.id')
				->innerJoin('user','responsible.user_id = user.id')
				->andWhere(['user.id'=>$this->responsible_user]);
			;
        return $dataProvider;
    }
}
