<?php
return array (
	'Create Call Request'=>'Cоздать заявку',
	'Update Call Request'=>'Редактирование заявки',
	'Call Requests'=>'Заявки',
	'Call Request'=>'Заявка',
	'Done Request'=>'Закрытие Заявки',
	'Address'=>'Адрес',
	'Type'=>'Тип',
	'Time Add'=>'Время создания',
	'Time Performed'=>'Закрыто',	
	
	'Date Expected'=>'Дата выполнения',
	'Time Expected'=>'Время выполнения',
	'Time Expected Start'=>'Время выполнения',
	'Time Expected End'=>'Время выполнения(завершение)',

	'Time Start'=>'Начало(ч)',
	'Time End'=>'Завершение(ч)',
	
	'History corrections'=>'История изменений',
	'Call Types'=>'Типы заявок',
	'Create Call Type'=>'Создать тип заявок',
	'Name'=>'Имя',
	
	'Phone'=>'Телефон',
	'Contact'=>'Контакт',
	'Phone1'=>'Телефон',
	'Contact1'=>'Контакт',
	
	'Comment'=>'Комментарий',
	'Status'=>'Статус',
	'Responsible User'=>'Ответственный',
	'Check'=>'Проверить',
	'Addresses'=>'Адреса',

	'City'=>'Город',
	'Street'=>'Улица',
	'House'=>'Дом',
	'Room'=>'Квартира',
	
	'Cities'=>'Города',
	'Streets'=>'Улицы',
	'Houses'=>'Дома',
	
	'-select-'=>' ',
	'Posible Dependency Address'=>'Зависимость от адресов',
	'Bind Crash'=>'Связать с аварией',
	'Comment Done'=>'Комментарий',
	'Dependency ID'=>'Зависим от адреса',
	
	'Done'=>'Закрыть заявку',
	'Open'=>'Открыть заявку',

	'Create'=>'Создать',
	'Create'=>'Создать',
	'Create'=>'Создать',
	'Create House'=>'Создать Дом',
	'Create Street'=>'Создать Улицу',
	'Create City'=>'Создать Город',
	'Update Address: '=>'Обновить Адрес',
	'Update'=>'Обновить',
	'Time Modified'=>'Изменено',
	'User Name r'=>'Отредактировал',
	'Type crash error' => 'Заявка с типом "Авария" не может относиться к другой аварии',
	'Are you sure you want to delete this item?' => 'Вы действительно хотите удалить этот элемент?',
);
?>