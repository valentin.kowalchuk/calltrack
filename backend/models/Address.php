<?php

namespace backend\models;

use yii\helpers\ArrayHelper;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $type
 * @property string $name
 * @property integer $dependency_id
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
public $city;

    public $street;
    public $house;
    public $str_address = '';
    public $crash_info = [];
    public $a_dependency = [];
	
	 
    public static function tableName()
    {
        return 'address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'type', 'name'], 'required'],
            [['dependency_id'], 'safe'],
            [['id', 'parent_id', 'type', 'dependency_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            // 'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'type' => Yii::t('app', 'Type'),
            'name' => Yii::t('app', 'Name'),
            'dependency_id' => Yii::t('app', 'Dependency ID'),
        ];
    }

	public function afterFind()
	{
	}

	public function getStrName($id)
	{
		$addr = Address::find()->where(['id'=>$id])->one();
		return (isset($addr->name)) ? $addr->name : '';
	}
	
	public function getStrAddress($room = '')
	{
		return implode(ArrayHelper::map( $this->getParents($this->id), 'id', 'name'), ' ');// . ', ' . $room;
	}

    public function getChildrens($id = 0, $f = false)
    {
		if (is_null($id)) return [];
        $result = Address::find()->where(['parent_id' => $id])->all();
		if($f)
			array_unshift($result, ['id'=>0, 'name'=>Yii::t('app', '-select-')]);
        return $result;
    }	

    public function getParents($id = 0, $key = 'parent')
    {
		if ($key == 'parent') $key = 'parent_id';
		if ($key == 'dependency') $key = 'dependency_id';		
		$i=8;
		$result = [];
		while($i-->0 and $id>0)
		{
			$address = Address::find()->where(['id' => $id])->one();
			$id = $address[$key];
			if(is_object($address) and ($id!==$address->id))
			{
				$result[] = $address;
			}
		}
        return array_reverse($result);
    }
	
    public function getParentsId($id = 0, $key = 'parent')
    {
		$a_address = self::getParents($id, $key);
		if ($key == 'parent') $key = 'parent_id';
		if ($key == 'dependency') $key = 'dependency_id';		
		$a_id = ArrayHelper::map( $a_address, $key, 'id');
		if ($key == 'parent_id'){
			array_splice($a_id, 3);
			$a_id = array_pad($a_id, 3, null);
		}
		$result = $a_id;
		return $result;
	}

    public function getChildDependencies($id = 0, $f = null)
    {

    }

    public function getDependenciesId($id = 0)
    {

	}
}
