<?php

namespace app\models;

use backend\models\Address;
use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "call_request".
 *
 * @property integer $id
 * @property string $time_add
 * @property string $time_performed
 * @property integer $type
 * @property string $address
 * @property integer $phone
 * @property integer $contact
 * @property string $comment
 * @property integer $status
 * @property integer $responsible_user
 */
class CallRequest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'call_request';
    }

	public $responsible_user = [];
	public $responsible_user_info = [];
	public $city;
	public $street;
	public $house;

	public $str_address = '';
	public $crash_info = [];
	public $a_dependency = [];
	public $text;
	public $my_max = '2050-01-01 01:01:01';
	public $my_time = '';
	public $time_expected_hh_start = null;
	public $time_expected_hh_end = null;
	public $max_time_performed = null;
    /**
     * @inheritdoc
     */
    public function rules()
    {
		$this->my_time = date('Y-m-d H:i:s');
		$this->my_time = strtotime($this->my_time);

		$this->max_time_performed = date("Y-m-d H:i:s", $this->my_time);

		// $this->my_time = $this->my_time-(60*10);
		// Суть в том, что если мы выбираем, что заявка закрыта сейчас, то она не закрывается
		$this->my_time = $this->my_time-(60*10);
		$this->my_time = date("Y-m-d H", $this->my_time);
        return [
            [['type', 'contact'], 'required'], //, 'status'
       		// напомню что надо бы сделать чтобы заявки можно было принимать без указания номера телефона и даты. В случае аварий это актуально.— Игорь Семенченко, Вчерашние 16:17
            //[['phone'], 'required'],
			[['comment_done'], 'required', 'on'=>'done'],
			[['time_performed'], 'required', 'on'=>'done'],
			[['time_performed'], 'date', 'format' => 'yyyy-M-d H'],
			[['time_performed'], 'compare', 'compareAttribute'=>'time_expected_start', 'operator'=>'>=', 'on'=>['done'], 'message' => 'Время закрытия должно быть больше запланированного времени начала'],
			// [['time_performed'], 'compare', 'compareAttribute'=>'max_time_performed', 'operator'=>'<=', 'on'=>['done'], 'message' => 'Время закрытия не должно быть больше текущего'],

			[['time_add', 'crash_id'], 'safe'],
            [['time_expected_start','time_expected_end'], 'safe'],
            [['type', 'status'], 'integer'],
			[['phone', 'phone1'], 'integer'],
            [['comment', 'contact', 'contact1'], 'string'],

            [['house'], 'integer'],
            [['room'], 'string', 'max' => 6],

			[['time_expected_start','time_expected_end'], 'date', 'format' => 'yyyy-M-d H'],
			[['time_expected_start'/*,'time_expected_end'/**/], 'compare', 'compareAttribute'=>'my_time', 'operator'=>'>=', 'on'=>['create','update']/**/, 'message' => 'Время должно быть больше текущего'],
			[['time_expected_start','time_expected_end'], 'compare', 'compareAttribute'=>'my_max', 'operator'=>'<='],
       		// напомню что надо бы сделать чтобы заявки можно было принимать без указания номера телефона и даты. В случае аварий это актуально.— Игорь Семенченко, Вчерашние 16:17
			//[['time_expected_start'/*,'time_expected_end'/**/], 'required'],

			[['time_expected_hh_start'], 'safe'],
			[['time_expected_hh_end'], 'safe'],
        ];
    }

    public function init()
    {
	    parent::init();
	}

	public function getCrashInfo()
	{
		if ($this->crash_id)
			return $this->crash_info = CallRequest::find()->where(['id' => $this->crash_id])->one();
	}

	public function loadAddress()
	{
		$a_address = Address::getParentsId($this->address_id);
		list($this->city, $this->street, $this->house) = $a_address;
	}

	public function canClose()
	{
		return (CallRequest::find()->where(['crash_id'=>$this->id])->andWhere(['time_performed' => null])->count()==0);
	}

	public function canEdit()
	{
		return ((CallRequest::find()->where(['id'=>$this->id])->andWhere(['time_performed' => null])->count()>0) or (empty($this->id)));
	}

	public function beforeValidate()
	{
		$this->address_id = $this->house;
		$this->loadAddress();
		$result1 = true;
		if ($this->crash_id and ($this->type == 1))
		{
			$this->addError('type', Yii::t('app', 'Type crash error'));
			$result1 = false;
		}

		if(\Yii::$app->controller->action->id == 'done')
		{
			if (!$this->canClose())
			{
				$this->addError('closed', Yii::t('app', 'Нельзя закрыть, т.к. есть незакрытые подзаявки'));
				$result1 = false;
			}
		}

		if(($this->time_expected_start>$this->time_expected_end) and (!empty($this->time_expected_end)))
		{
			$this->addError('time_expected_start', Yii::t('app', 'Время завершения выполнения заявки должно быть больше его начала'));
		}

		$result = parent::beforeValidate();
		return $result and $result1;
	}

	public function afterFind()
	{
		$this->responsible_user_info = $this->getResponsibleUsers();
		$this->responsible_user = ArrayHelper::map($this->responsible_user_info, 'id', 'id');
		$this->loadAddress();

		if(!empty($this->time_expected_start))
		{
			$this->time_expected_start = date("Y-m-d H", strtotime($this->time_expected_start));
			$this->time_expected_hh_start = date("H", strtotime($this->time_expected_start.':00:00'));
		}

		if(!empty($this->time_expected_end))
		{
			$this->time_expected_end = date("Y-m-d H", strtotime($this->time_expected_end));
			$this->time_expected_hh_end = date("H", strtotime($this->time_expected_end.':00:00'));
		}
	}

	public function beforeSave($insert)
	{
		if(($this->time_expected_start==$this->time_expected_end) or (empty($this->time_expected_end)))
			$this->time_expected_end = null;

		if (empty($this->time_performed)) $this->time_performed = null;
		if (empty($this->time_add)) $this->time_add = date('Y-m-d H:i:s');
		$this->address_id = $this->house;
		$address = \backend\models\Address::find()->where(['id'=>$this->house])->one();
		if (gettype($address) == 'object')
			$this->address = $address->getStrAddress();//->id;
		else
			$this->address = null;
		if (!empty($this->address) and !empty($this->room))
			$this->address .= ', ';
		$this->address .= $this->room;

		if (in_array($this->type, [2,15]))
			$this->city = $this->street = $this->house = $this->room = $this->address = $this->address_id = null;

		return true;
	}

    public function getResponsibleUsers()
    {
		$query = new Query;
		return $query->select('user.*')->from('call_request')
			->innerJoin('responsible','responsible.call_request_id = call_request.id')
			->innerJoin('user','responsible.user_id = user.id')
			->where(['call_request.id'=>$this->id])->all();
	}

    public function setResponsibleUser($user_id)
    {
		$db = new Query;
		if(!empty($user_id))
			$db->createCommand()->insert('responsible', ['user_id' => (int)$user_id, 'call_request_id' => (int)$this->id])->execute();
	}

    public function setResponsibleUsers($a_id)
    {
		$db = new Query;
		$db->createCommand()->delete('responsible', ['call_request_id' => $this->id])->execute();
		if(is_array($a_id))
			foreach ($a_id as $user_id)
				if(!empty($user_id))
					$db->createCommand()->insert('responsible', ['user_id' => (int)$user_id, 'call_request_id' => (int)$this->id])->execute();
	}

    public function deleteResponsibleUsers($a_id)
    {
		$db = new Query;
		if(is_array($a_id))
			$db->createCommand()->delete('responsible', ['user_id' => $a_id, 'call_request_id' => $this->id])->execute();
	}

	public function getPosibleDependencyAddress($id) //address_id
	{
		$a_address_id = Address::getParentsId($id, 'dependency');
		return CallRequest::find()->where(['address_id' => $a_address_id,/**/ 'type' => 1 /**/])
			->andWhere(['time_performed' => null])
			->all();
	}

    public function getTypesNames()
    {
		return \yii\helpers\ArrayHelper::getColumn(\backend\models\CallType::find()->where(['id' => $this->type])->all(), 'name');
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'time_add' => Yii::t('app', 'Time Add'),
			'time_expected' => Yii::t('app', 'Time Expected'),
            'time_expected_start' => Yii::t('app', 'Time Expected Start'),
            'time_expected_end' => Yii::t('app', 'Time Expected End'),
            'time_add' => Yii::t('app', 'Time Add'),
            'time_performed' => Yii::t('app', 'Time Performed'),
            'type' => Yii::t('app', 'Type'),
            'address' => Yii::t('app', 'Address'),
            'city' => Yii::t('app', 'City'),
            'street' => Yii::t('app', 'Street'),
            'house' => Yii::t('app', 'House'),
            'room' => Yii::t('app', 'Room'),
            'phone' => Yii::t('app', 'Phone'),
            'contact' => Yii::t('app', 'Contact'),
            'phone1' => Yii::t('app', 'Phone1'),
            'contact1' => Yii::t('app', 'Contact1'),
            'comment' => Yii::t('app', 'Comment'),
            'comment_done' => Yii::t('app', 'Comment Done'),
            'status' => Yii::t('app', 'Status'),
            'responsible_user' => Yii::t('app', 'Responsible User'),
        ];
    }

	public function afterSave($insert, $changedAttributes)
	{
		parent::afterSave($insert, $changedAttributes);
		$temp = serialize($this);
		$db = new Query;
		$db->createCommand()->insert('call_request_log', ['data_id' => $this->id, 'data' => $temp, 'user_id' => \Yii::$app->user->getId()])->execute();
	}

	public function getLog($id)
	{
		$query = new Query;
		$temp = "";
		$temp = $query->select('*')->from('call_request_log')
			->where(['data_id'=>$id])->all();
		$my = $user = $time = array();

		foreach ($temp as $key => $value) {
			$my[$key] = $temp[$key]['data'];
			$user[$key] = $temp[$key]['user_id'];
			$time[$key] = $temp[$key]['time'];
			$my[$key] = unserialize($my[$key]);
		}

		$usernames = $query->select('id, userfullname')->from('user')
			->where('')->all();
		$usernames = ArrayHelper::map($usernames, 'id', 'userfullname');

		$my = ArrayHelper::toArray($my, [
            'app\models\CallRequestLog' => [
            'id',
            'time_add',
            'time_performed',
            'type',
            'address',
            'phone',
            'contact',
            'comment:ntext',
            'status',
            ],
        ]);
		foreach ($user as $key => $value){
			$temp = $user[$key]/*+1*/;
			if(array_key_exists($temp, $usernames)){
				$my[$key]['user_name'] = $usernames[$temp];
			}
			$my[$key]['time'] = $time[$key];
		}
        $result = $my;
		return $result;
	}
}