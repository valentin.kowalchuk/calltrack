<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "call_request_log".
 *
 * @property integer $id
 * @property integer $data_id
 * @property string $time
 * @property string $data
 */
class CallRequestLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'call_request_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data_id', 'data'], 'required'],
            [['data_id'], 'integer'],
            [['time'], 'safe'],
            [['data'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'data_id' => Yii::t('app', 'Data ID'),
            'time' => Yii::t('app', 'Time'),
            'data' => Yii::t('app', 'Data'),
        ];
    }
}
