<?php

namespace backend\models;
use yii\helpers\ArrayHelper;

use Yii;

/**
 * This is the model class for table "call_type".
 *
 * @property integer $id
 * @property string $name
 * @property string $color
 */
class CallType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'call_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['color'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'color' => Yii::t('app', 'Color'),
        ];
    }
	
    public function beforeDelete()
    {
		return $this->canDelete();
	}
	
    public function getAllItems()
    {
        $result = ArrayHelper::map(\backend\models\CallType::find()->all(), 'id', 'name');//orderBy('parent_id')->
		return $result;
    }

    public function getColors()
    {
        $result = ArrayHelper::map(\backend\models\CallType::find()->all(), 'id', 'color');//orderBy('parent_id')->
		return $result;
    }

    public function getAll()
    {
		//$result = ArrayHelper::index(\app\models\Categories::find()->orderBy('parent_id')->all(), 'id');
    }

    public function canEdit()
    {
		return true;
    }
	
    public function canDelete()
    {
		if ($this->id<4)
			return false;
        if (\app\models\CallRequest::find()->where(['type' => $this->id])->count())
			return false;
		return true;
    }
}