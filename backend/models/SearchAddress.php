<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Address;
//use backend\models\CallRequest;

/**
 * SearchAddress represents the model behind the search form about `\backend\models\Address`.
 */
class SearchAddress extends Address
{
    /**
     * @inheritdoc
     */


    public function rules()
    {
        return [
            [['id', 'parent_id', 'type', 'dependency_id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function search($params)
    {
        $query = Address::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'type' => $this->type,
            'dependency_id' => $this->dependency_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
     public function parents($id)
    {
        $i = 5;
        $result = array();
        while ($id>0&&$i>0) {
            $i--;
            $temp = SearchAddress::find()->where(['id' => $id])->one();
            $result[] = $temp;
            $id = $temp['parent_id'];
        }
        return $result;
    }


     public function children($id)
    {
        static $i = 5;
        static $result = array();
        $temp = SearchAddress::find()->where(['parent_id' => $id])->all();
        if($i>0)
        foreach ($temp as $key => $v) {

                $result[] = $v['parent_id'];
                //print_r($result);
                $i--;
                self::children($v['id']);
        }
        return $result;
    }



     public function parentsDep($dependency_id)
    {
        $i = 5;
        $result = array();
        while ($dependency_id>0&&$i>0) {
            $i--;
            $temp = SearchAddress::find()->where(['id' => $dependency_id])->one();
            $result[] = $temp;
            $id = $temp['dependency_id'];
        }
        return $result;
    }
     public function childrenDep($id)
    {
        static $i = 5;
        static $result = array();
        $temp = SearchAddress::find()->where(['dependency_id' => $id])->all();
        if($i>0)
        foreach ($temp as $key => $v) {

                $result[] = $v['dependency_id'];
                $i--;
                self::children($v['id']);
        }
        return $result;
    }
    public function canDelete($id){
        $req = \backend\controllers\RequestSearch::find()->where(['address_id' => $id])->count();
        $temp1 = SearchAddress::childrenDep($id);
        $depchild = !empty($temp1);
        $temp2 = SearchAddress::children($id);
        $haschildren = !empty($temp2);

        return $result = ($req > 0 || $depchild || $haschildren) ? false : true;

    }
}
