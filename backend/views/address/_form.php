<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Address;
use backend\models\SearchAddress;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Address */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="address-form">
    
    <?php if (!isset($parent_id)){
        $parent_id = 0;
        } ?>
    <?php $form = ActiveForm::begin(); ?>

    <?//= $form->field($model, 'parent_id')->textInput() ?>
    <?= $form->field($model, 'parent_id')->hiddenInput(['value' => $parent_id])->label(false) ?>
    <?= $form->field($model, 'type')->hiddenInput(['value' => $type])->label(false) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    
    
    <?php if($type>2){ ?>
        <?= $form->field($model, 'dependency_id')->hiddenInput()->label(Yii::t('app', 'Dependency ID')) ?>
        <?php $this->registerJsFile(
            '@web/js/drop_down.js',
            ['depends' => 'yii\web\YiiAsset' ]
            );
       ?>
    <?= Html::hiddenInput('path_2', Url::toRoute('request/getaddrinp'), ['id' => 'j_path_addr_inp']) ?>
    <div class="row j_address">
        <div class="col-md-2 j_col_addr">
            <?=$form->field($model, 'city')->listBox(ArrayHelper::map(Address::getChildrens(0, true), 'id', 'name'), ['maxlength' => 255, 'size'=>'1'])?>
        </div>
        <div class="col-md-4 j_col_addr">
            <?=$form->field($model, 'street')->listBox(ArrayHelper::map(Address::getChildrens($model->city, true), 'id', 'name'), ['maxlength' => 255, 'size'=>'1'])?>
        </div>
        <div class="col-md-2 j_col_addr">
            <?=$form->field($model, 'house')->listBox(ArrayHelper::map(Address::getChildrens($model->street, true), 'id', 'name'), ['maxlength' => 255, 'size'=>'1'])?>
        </div>
    </div>
    <?php } else { ?>
         <?= $form->field($model, 'dependency_id')->hiddenInput()->label(false) ?>
<?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
