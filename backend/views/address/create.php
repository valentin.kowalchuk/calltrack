<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Address */

$Addresses = ['', 'City', 'Street', 'House'];
// $this->title = Yii::t('app', 'Create Address');
$this->title = Yii::t('app', 'Create '.$Addresses[$type]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', $Addresses[$type]), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Addresses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="address-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'type' => $type,
        'parent_id' => $parent_id,
    ]) ?>

</div>
