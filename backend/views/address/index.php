<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use backend\models\SearchAddress;
use backend\controllers\AddressController;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchAddress */
/* @var $dataProvider yii\data\ActiveDataProvider */
/*if(empty($type))
	$type = 1;/**/
$word = ['', 'City', 'Street', 'House'];
$words = ['', 'Cities', 'Streets', 'Houses'];
$bwords = ['', 'Create City', 'Create Street', 'Create House'];
$this->title = Yii::t('app', $words[$type]);
//$this->title = Yii::t('app', 'Addresses');
//$this->title = Yii::t('app', 'Addresses');
$this->params['breadcrumbs'] = [];
if(!empty($current['name'])) $this->params['breadcrumbs'][] = $current['name'];
?>
<? //= Html::a($current['name'], ['index', 'parent_id' => $parent_id/*, 'type' => $type,/**/], ['class' => 'btn btn-success']) /**/?>
<?
$this->params['breadcrumbs'][] = Yii::t('app', $words[$type]) ;
//$this->params['breadcrumbs'][] = $this->title;

?>
<div class="address-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a(Yii::t('app', $bwords[$type]), ['create', 'parent_id' => $parent_id/*, 'type' => $type,/**/], ['class' => 'btn btn-success']) /**/?>
    </p>
        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            // 'name',
            [
                'attribute' => 'name',
                'value' => function ($data) {
                    if($data->type<3){
                    return Html::a(Html::encode($data->name), Url::to(['index', 'parent_id' => $data->id, 'type' => $data->type+1]));}
                    else {return Html::encode($data->name);}
                },
                'format' => 'raw',
            ],
            //'parent_id',
            //'dependency_id',
            [
                'attribute' => 'dependency_id',
                'value' => function ($data) {
                    return Html::a(Html::encode(backend\controllers\AddressController::getFullPath($data->dependency_id)), Url::to(['address/view', 'id' => $data->dependency_id,]));
                },
                'format' => 'raw',
            ],

            [
                'class' => 'app\widgets\AddressActionColumn',
                'template' => '<p style="white-space: nowrap;">{update} {view} {delete}</p>',
                'parent_id' => $parent_id,
                'type' => $type,
                // 'name' => $name,
                'buttons' => [
                    'update' => function($url, $model) {
                        $url = \yii\helpers\Url::toRoute(['/address/update', 'id' => $model->id/**/]);
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => 'Редактирование',
                            'data-pjax' => '0',             // отключение стандартного обработчика pjax. Он все портит:)
                            /*'class' => 'j_edit_request' /**/
                        ]);
                    },
                    'view' => function($url, $model) {
                        $url = \yii\helpers\Url::toRoute(['/address/view', 'id' => $model->id]);
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => 'Просмотр',
                            'data-pjax' => '0',
                            /*'class' => 'j_view_request'/**/
                        ]);
                    },
                    'delete' => function($url, $model) {
                        $url = \yii\helpers\Url::toRoute(['/address/delete', 'id' => $model->id]);
                        if(SearchAddress::canDelete($model->id)){
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => 'Удалить',
                            'data-pjax' => '0',
                            /*'class' => 'j_done_request'/**/
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ]);
                        }
                        else return;

                    },
                ],
                'contentOptions' => array('class' => 'col-xs-1', 'align'=>'center'),
            ]
        ],
    ]); ?>
</div>
