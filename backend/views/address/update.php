<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Address */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Address',
]) . ' ' . $model->name;
$Addresses = ['', 'City', 'Street', 'House'];

$this->title = Yii::t('app', 'Update') . ' ' . Yii::t('app', $Addresses[$type]). ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', $Addresses[$type]), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Addresses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

/*$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');/**/
?>
<div class="address-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'parent_id' => $parent_id,
        'type' => $type,
    ]) ?>

</div>
