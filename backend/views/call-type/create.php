<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CallType */

$this->title = Yii::t('app', 'Create Call Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Call Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="call-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
