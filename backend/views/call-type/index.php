<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CallTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Call Types');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="call-type-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a(Yii::t('app', 'Create Call Type'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'rowOptions' => function($model, $key, $index, $grid){
			return ['class' => 'color_rec_'.$model->id];
		},
        'columns' => [
			[
				'attribute'=>'id',
				'contentOptions' => array('width' => '5%', 'align'=>'center'),
            ],
			[
				'attribute'=>'name',
			],

		
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '<p style="white-space: nowrap;">{update} {delete}</p>',
				'buttons' => [
					'update' => function($url, $model) {
						if (!$model->canEdit())
							return;
						$url = \yii\helpers\Url::toRoute(['/call-type/update', 'id' => $model->id]);
						return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
							'title' => 'Редактирование',
							'data-pjax' => '0',
						]);
					},
					'delete' => function($url, $model) {
						if (!$model->canDelete())
							return;
						$url = \yii\helpers\Url::toRoute(['/call-type/delete', 'id' => $model->id]);
						return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
							'aria-label'=>"Удалить",
							'data-confirm'=>"Вы уверены, что хотите удалить этот элемент?",
							'data-method'=>"post",
							'data-pjax'=>"0",						
							'title' => 'Удалить',
							'data-pjax' => '0',	
						]);
					},
				],
				'contentOptions' => array('class' => 'col-xs-1', 'align'=>'center'),
			]			
        ],
    ]); ?>
</div>