<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

use webvimark\modules\UserManagement\components\GhostMenu;
use webvimark\modules\UserManagement\UserManagementModule;
/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
backend\assets\AdminAsset::register($this);

$roles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());
$roles = yii\helpers\ArrayHelper::getColumn($roles, 'name');
$roles = implode($roles, ' ');
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?= Html::hiddenInput('my-path', Url::toRoute('request/getaddrinp'), ['id' => 'j_path_addr_inp']) ?>
    <?= Html::hiddenInput('my-path', Url::toRoute('request/view-posible-dependency'), ['id' => 'j_view_posible_dependency_inp']) ?>
    <?= Html::hiddenInput('my-path', Url::toRoute('request/get-sub-crash'), ['id' => 'j_view_get_sub_crash']) ?>
    <?php $this->beginBody() ?>
	<?//=Yii::$app->hasRole('group')?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => '<img src="'.Url::to('img/logocallcenter.jpg').'" class="img img-responsive" style="width:251px; height:auto;">',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                   ],
            ]);
			
            $menuItems = [
                //['label' => 'Requests', 'url' => ['/request/index']],
					['label' => 'Заявки', 'url' => ['/request/index']],
            ];
			if (Yii::$app->user->can('request_type_edit'))
				$menuItems[] = ['label' => 'Типы заявок', 'url' => ['/call-type/index']];
			if (Yii::$app->user->can('address_edit'))
				$menuItems[] = ['label' => 'Адреса', 'url' => ['/address/index']];
			if (Yii::$app->user->can('аdmin'))
				$menuItems[] = ['label' => 'Пользователи и права', 'url' => ['/user-management/user/index']];
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
            } else {
                $menuItems[] = [
                    'label' => 'Logout (' . Yii::$app->user->identity->username . ')' . " [{$roles}]",
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-left'],
                'items' => $menuItems,
            ]);
            NavBar::end();
        ?>

        <div class="my-container">
		<pre></pre>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

	  <?if(Yii::$app->controller->module->id == 'user-management'){?>
		<div class="col-md-3">
		<?		
		echo GhostMenu::widget([
			'encodeLabels'=>false,
			'activateParents'=>true,
			'options' => [
				'class'=>'role-menu',
			],
			'items' => [
				[
					'label' => 'Админская часть',
					'items'=>UserManagementModule::menuItems()
				],
				[
					'label' => 'Frontend routes',
					'items'=>[
						['label'=>'Login', 'url'=>['/user-management/auth/login']],
						['label'=>'Logout', 'url'=>['/user-management/auth/logout']],
						['label'=>'Registration', 'url'=>['/user-management/auth/registration']],
						['label'=>'Change own password', 'url'=>['/user-management/auth/change-own-password']],
						['label'=>'Password recovery', 'url'=>['/user-management/auth/password-recovery']],
						['label'=>'E-mail confirmation', 'url'=>['/user-management/auth/confirm-email']],
					],
				],
			],
		]);/**/
		?>
        </div>
		<?}?>
		
		<?if(Yii::$app->controller->module->id == 'user-management'){?>
		<div class="col-md-9">
		<?}?>
			<?= $content ?>
		<?if(Yii::$app->controller->module->id == 'user-management'){?>
		</div>
		<?}?>
		</div>
    </div>

	<footer class="footer">
		<div class="container">
			<p class="pull-left">&copy; SkyGroup Ltd.</p>
			<p class="pull-right">Powered by <a rel="external" href="http://www.techworldsoft.com/">TechWorld Software</a></p>
		</div>
	</footer>

	<div class="modal fade" id="modal-req" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	   <div class="modal-dialog modal-sm_">
		 <div class="modal-content">
		   <div class="modal-body">
			 ...
		   </div>
		 </div><!-- /.modal-content -->
	   </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<div class="modal fade" id="modal-check-crash" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	   <div class="modal-dialog modal-sm_">
		 <div class="modal-content">
		   <div class="modal-body">
			 ...
		   </div>
		 </div><!-- /.modal-content -->
	   </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
