<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Address;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\CallRequest */
/* @var $form yii\widgets\ActiveForm */
?>
<?//=Url::to(['address/get_addr_inp'])?>
<?//= Html::hinput(Yii::t('app', 'Check') , ['class' => 'btn btn-primary', 'id' => 'j_check']) ?>
<?//=Html::input('jhuh', Url::to('get_addr_inp'), ['id' => 'getpath']) ?>
<?= Html::hiddenInput('my-path', $model->id, ['id' => 'j_model_id']) ?>
<?$disabled_class = (!$model->canEdit()) ? 'disabled_form' : ''?>

<?php $form = ActiveForm::begin(['options' => ['class' => 'request_form '.$disabled_class]]); ?>

<?
?>
 <div class="modal-body">
 
	<div class="row j_trash <?if(!$model->crash_id){?>hide<?}?>">
		<div class="col-md-12">
			<?=$form->field($model, 'crash_id')->hiddenInput()->label(false);?>
	
			<div class="alert alert-danger j_trash_info">
				<h4>Заявка относится к аварии #<span class="j_trash_info_id"><?if($model->crash_id){?><?=$model->getCrashInfo()->id?><?}?></span></h4>
				<p class="text-uppercase j_trash_info_address"><?if($model->crash_id){?><?=$model->getCrashInfo()->address?><?}?></p>
				<p class="j_trash_info_comment"><?if($model->crash_id){?><?=$model->getCrashInfo()->comment?><?}?><p>
			</div>
	
		</div>
	</div>

	<?= $form->field($model, 'type')->listBox(\backend\models\CallType::getAllItems(), ['maxlength' => 255, 'size'=>'1']) ?>
    <?//= $form->field($model, 'time_add')->textInput(['class' => 'form-control j_datetimepicker']) ?>

	<?/*if(Yii::$app->user->can('operator')){?>
		<?= $form->field($model, 'responsible_user')->listBox(\backend\models\User::getAllItems(), ['maxlength' => 25, 'size'=>'1']) ?>
	<?}/**/?>

	<div class="row">
		<div class="hidden">
			<?=$form->field($model, 'time_expected_start')->textInput(['class' => 'form-control'])?>
			<?=$form->field($model, 'time_expected_end')->textInput(['class' => 'form-control'])//['class' => 'hidden']?>
		</div>
		<div class="col-md-6">
			<?=Html::label(Yii::t('app', 'Date Expected'))?>
			<?//=gettype($model->time_expected_start)?>
			<?=\Yii::$app->controller->renderPartial('inp_datetimepicker', ['m'=>Html::input('text', 'date_expected_d', $model->time_expected_start, ['class' => 'form-control', 'id'=>'date_expected_d']), 'class'=>'j_datetimepicker_h']);?>
		</div>
		<div class="col-md-3">
			<?=Html::label(Yii::t('app', 'Time Start'))?>
			<?=Html::input('text', 'CallRequest[time_expected_hh_start]', $model->time_expected_hh_start/*date("H", strtotime($model->time_expected_start.':00:00'))/*$model->time_expected_hh_start/**/, ['class' => 'form-control', 'id'=>'time_expected_hh_start', 'list'=>'h_list_1'])?>
			<datalist id="h_list_1">
				<?for($i=1;$i<=24;$i++){?>
				<option><?=$i?></option>
				<?}?>
			</datalist>
		</div>
		<div class="col-md-3">
			<?=Html::label(Yii::t('app', 'Time End'))?>
			<?=Html::input('text', 'CallRequest[time_expected_hh_end]', $model->time_expected_hh_end/*$model->time_expected_hh_end/**/, ['class' => 'form-control', 'id'=>'time_expected_hh_end', 'list'=>'h_list_2'])?>
			<datalist id="h_list_2">
				<?for($i=1;$i<=24;$i++){?>
				<option><?=$i?></option>
				<?}?>
			</datalist>
			<?//=\Yii::$app->controller->renderPartial('inp_datetimepicker', ['m'=>$form->field($model, 'time_expected_end')->textInput(['class' => 'form-control'])]);?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group field-callrequest-contact required has-error">
				<div class="help-block"><?=implode(array_merge($model->getErrors('time_expected_start'),$model->getErrors('time_expected_end')), ', ')?></div>
			</div>
		</div>
	</div>

	<?/*?>
	<pre><?=print_r($model,1)?></pre>
	<?/**/?>
	<?/*time_expected_end
	time_expected_start
	time_performed
    <?//= $form->field($model, 'type')->textInput() ?>
	
	<?/*=$form->field($model, 'type')->checkboxList(\app\models\Categories::getAllItems(), ['maxlength' => 255, 'size'=>'1', 'separator' => '<span><br></span>',
	    '_item' => function ($index, $label, $name, $checked, $value)
		{
			return $this;
		}	
	])/**/?>
	<?//= backend\controllers\AddressController::getFullPath($model->address_id);?>
	<?//= backend\controllers\CallRequest::find([$model->crash_id]);?>
	<?//if(model->type<3)?>
	
	<div class="row j_address">
			<?//= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
			<?//= $form->field($model, 'type')->listBox(\backend\models\CallType::getAllItems(), ['maxlength' => 255, 'size'=>'1']) ?>
		<div class="col-md-2 j_col_addr">
			<?$list_data=Address::getChildrens(0, true);?>
			<?$addr_x = 'city'?>

			<?=$form->field($model, $addr_x)->listBox(ArrayHelper::map($list_data, 'id', 'name'), ['maxlength' => 255, 'size'=>'1'])->label(false)?>
			<?=Html::label(Yii::t('app', 'City'))?>
			<?=Html::tag('input', '', ['type'=>'text', 'class' => 'inp_sel form-control', 'list'=>"{$addr_x}_list", 'value'=>Address::getStrName($model->$addr_x)])?>
			<?=Html::beginTag('datalist', ['class' => 'modal-dialog', 'id'=>"{$addr_x}_list"])?>
				<?=Html::renderSelectOptions($model->$addr_x, ArrayHelper::map($list_data, 'name', 'name'))?>
			<?=Html::endTag('datalist');?>
		</div>
		<div class="col-md-4 j_col_addr">
			<?$list_data=Address::getChildrens($model->city, true);?>
			<?$addr_x = 'street'?>

			<?=$form->field($model, $addr_x)->listBox(ArrayHelper::map($list_data, 'id', 'name'), ['maxlength' => 255, 'size'=>'1'])->label(false)?>
			<?=Html::label(Yii::t('app', 'Street'))?>
			<?=Html::tag('input', '', ['type'=>'text', 'class' => 'inp_sel form-control', 'list'=>"{$addr_x}_list", 'value'=>Address::getStrName($model->$addr_x)])?>
			<?=Html::beginTag('datalist', ['class' => 'modal-dialog', 'id'=>"{$addr_x}_list"])?>
				<?=Html::renderSelectOptions($model->$addr_x, ArrayHelper::map($list_data, 'name', 'name'))?>
			<?=Html::endTag('datalist');?>

			<?/*=$form->field($model, 'street')->listBox(ArrayHelper::map(Address::getChildrens($model->city, true), 'id', 'name'), ['maxlength' => 255, 'size'=>'1'])?>
			<?$addr_x = 'street'?>
			<?=Html::tag('input', '', ['type'=>'text', 'class' => 'inp_sel form-control', 'list'=>"{$addr_x}_list", 'value'=>Address::getStrName($model->$addr_x)])?>
			<?=Html::beginTag('datalist', ['class' => 'modal-dialog', 'id'=>"{$addr_x}_list"])?>
				<?=Html::renderSelectOptions($model->$addr_x, ArrayHelper::map(Address::getChildrens($model->city, true), 'name', 'name'))?>
			<?=Html::endTag('datalist');/**/?>
		</div>
		<div class="col-md-2 j_col_addr">
			<?$list_data=Address::getChildrens($model->street, true);?>
			<?$addr_x = 'house'?>

			<?=$form->field($model, $addr_x)->listBox(ArrayHelper::map($list_data, 'id', 'name'), ['maxlength' => 255, 'size'=>'1'])->label(false)?>
			<?=Html::label(Yii::t('app', 'House'))?>
			<?=Html::tag('input', '', ['type'=>'text', 'class' => 'inp_sel form-control', 'list'=>"{$addr_x}_list", 'value'=>Address::getStrName($model->$addr_x)])?>
			<?=Html::beginTag('datalist', ['class' => 'modal-dialog', 'id'=>"{$addr_x}_list"])?>
				<?=Html::renderSelectOptions($model->$addr_x, ArrayHelper::map($list_data, 'name', 'name'))?>
			<?=Html::endTag('datalist');?>

			<?//=$form->field($model, 'house')->listBox(ArrayHelper::map(Address::getChildrens($model->street, true), 'id', 'name'), ['maxlength' => 255, 'size'=>'1'])?>
			<?//backend\controllers\AddressController::getFullPath($model->address)?>			
		</div>
		<div class="col-md-2">
			<?= $form->field($model, 'room')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-2">
			<br>
			<?= Html::button(Yii::t('app', 'Check') , ['class' => 'btn btn-primary', 'id' => 'j_check']) ?>
		</div>
	</div>

	<div class="row">
		<div class="phone col-md-4">
		<?= $form->field($model, 'phone')->textInput() ?>
		</div>
		<div class="contact col-md-8">
		<?= $form->field($model, 'contact')->textInput() ?>
		</div>
	</div>

	<div class="row">
		<div class="phone col-md-4">
		<?= $form->field($model, 'phone1')->textInput() ?>
		</div>
		<div class="contact col-md-8">
		<?= $form->field($model, 'contact1')->textInput() ?>
		</div>
	</div>
	
    <?= $form->field($model, 'comment')->textarea(['rows' => 3, 'cols'=> 30]) ?>
<?if(!$model->canEdit()){?>
    <?= $form->field($model, 'comment_done')->textarea(['rows' => 3, 'cols'=> 30])->label('Комментарий при закрыти'); ?>
<?}?>
    <?//= $form->field($model, 'status')->textInput() ?>
	<?if(Yii::$app->user->can('operator')){?>
		<?//= $form->field($model, 'responsible_user')->checkBoxList(\backend\models\User::getAllItems(), ['maxlength' => 25, 'size'=>'1']) ?>
	<?}?>
	
	<?//$user_id = \Yii::$app->user->identity->id;?>
	<?//$user = \Yii::$app->authManager->getRolesByUser($user_id);?>
	<?//$user = (int)Yii::$app->user->can('operator');?>
	<?//print_r($user)?>

	<?//=Html::Tag('option', ['class' => 'inp_sel', 'list'=>"state_list"])?>
<?//=Html::Tag('input', ['class' => 'inp_sel', 'list'=>"state_list"])?>

<?/*	<input type="text" name="state" class="inp_sel" list="state_list">
	<datalist id="state_list">
	  <option value="AL">Алабама</option>
	  <option value="AK">Аляска</option>
	  <option value="AZ">Аризона</option>
	  <option value="AR">Арканзас</option>
	</datalist>
<?/**/?>
<?//=Html::endTag('datalist');?>

</div>
<div class="modal-footer">
<?if($model->canEdit()){?>
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
<?}?>
	<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
</div>
<?php ActiveForm::end(); ?>
