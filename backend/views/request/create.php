<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CallRequest */

$this->title = Yii::t('app', 'Create Call Request');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Call Requests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="call-request-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
