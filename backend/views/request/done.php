<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CallRequest */

$this->title = Yii::t('app', 'Done Request') . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Call Requests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span><span class="sr-only"><?Yii::t('app', 'Close')?></span>
	</button>
	<h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
</div>

<div class="j_msg"> </div> <!-- For message -->
<?$disabled_class = (!$model->canEdit()) ? 'disabled_form' : ''?>
<?php $form = ActiveForm::begin(['options' => ['class' => 'request_done_form '.$disabled_class]]); ?>
<div class="modal-body">
	<div class="call-request-view">
		<?= DetailView::widget([
			'model' => $model,
			'attributes' => [
				'id',
				'time_add',
				'time_performed',
				[
					'attribute' => 'type',
					'value' => @\backend\models\CallType::find()->where(['id'=>$model->type])->one()['name'],
				],
				'time_expected_start',
				'time_expected_end',
				'address',
				'phone',
				'contact',
				'phone1',
				'contact1',
				'comment:ntext',
				//'responsible_user',
			],
		]) ?>
	</div>
	<?if($model->canClose()){?>
		<div class="row">
			<div class="<?if(Yii::$app->user->can('operator')){?>col-md-6<?}else{?>col-md-12<?}?>">
				<?=\Yii::$app->controller->renderPartial('inp_datetimepicker', ['m'=>$form->field($model, 'time_performed')->textInput(['class' => 'form-control'])]);?>
				<?= $form->field($model, 'comment_done')->textarea(['rows' => 3]) ?>
			</div>
		
		<?if(Yii::$app->user->can('operator')){?>
			<div class="col-md-6">
			<?= $form->field($model, 'responsible_user')->checkBoxList(\backend\models\User::getAllItems(), ['maxlength' => 25, 'size'=>'1']) ?>
			</div>
		<?}?>
		</div>
	<?}?>	
</div>
<div class="modal-footer">
<?if($model->canClose()){?>
	<?if($model->canEdit()){?>
	<?//var_dump($mx=$model->canEdit())?>
		<?= Html::submitButton(Yii::t('app', 'Done'), ['class' => 'btn btn-primary j_btn_done']) ?>
	<?}else{?>
		<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button><?/**/?>
	<?}?>
<?}else{?>
	<h3>В заявке есть незакрытые подзаявки</h3>
<?}?>
	<?//<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button><?/**/?>
</div>
<?php ActiveForm::end(); ?>