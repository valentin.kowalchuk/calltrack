<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\controllers\RequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Call Requests');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="call-request-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
		<?if(Yii::$app->user->can('request_create')){?>
			<?= Html::a(Yii::t('app', 'Create Call Request'), ['create'], ['class' => 'btn btn-success signup', 'id' => 'j_create_request']) ?>
		<?}?>
    </p>

	<div class="table-responsive">
		<?= $this->render('sub_crash_tr', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);?>
	</div>

</div>