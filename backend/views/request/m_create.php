<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CallRequest */

$this->title = Yii::t('app', $title) . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Call Requests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span><span class="sr-only"><?Yii::t('app', 'Close')?></span>
	</button>
	<h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
</div>
<div class="j_msg"> </div> <!-- For message -->
<?= $this->render('_m_form', [
	'model' => $model,
]) ?>