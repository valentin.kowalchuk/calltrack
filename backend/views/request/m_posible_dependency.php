<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $model app\models\CallRequest */
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span><span class="sr-only"><?Yii::t('app', 'Close')?></span>
	</button>
	<h4 class="modal-title"><?//= Html::encode($this->title) ?><?if(!empty($a_dependency)){?><?=Yii::t('app', 'Bind Crash')?><?}?></h4>
</div>
<div class="j_msg"> </div> <!-- For message -->

<?php $form = ActiveForm::begin(['options' => ['class' => 'check_address_form']]); ?>
 <div class="modal-body">
	<div class="row">
		<div class="col-md-12">
			<?/*$a_dep = []; foreach ($a_dependency as $dep){?>
			<?}/**/?>
			<?if(empty($a_dependency)){?>Аварий, от которых могут быть неиспраности на данном адресе, неизвестно<?}?>
			<?$a_dependency[] = ['id'=>'', 'text'=>"<div class = \"col-sm-9\"><div>Не относить к аварии</div></div>"]?>
			<?=Html::radioList('dependency_address_id', 'selection_x', ArrayHelper::map($a_dependency, 'id', 'text'),
				[
					'class' => 'col-sm',
					'id' => 'dependency_address_id',
					'format' => 'raw',
					'item' => function ($index, $label, $name, $checked, $value) {
						return Html::radio($name, $checked, [
							'class' => 'col-sm-1',
							'value' => $value,
							'label' => $label,
						]);
					}
				]
			)?>
			
		</div>
	</div>
	
</div>

<div class="modal-footer">
	<?= Html::submitButton(Yii::t('app', 'Ok'), ['class' =>'btn btn-primary request_done_form']) ?>
	<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
</div>
<?php ActiveForm::end(); ?>

<div style="padding: 15px;">
<?if(!empty($a_address_history)){?>
	<h4>История по адресу</h4>
	<div class="row" style="max-height: 300px; overflow: auto;">
		<div class="col-md-12">
<?
			$provider = new ArrayDataProvider([
            'allModels' => $a_address_history,
            ]);
		$provider->pagination = false;
			?>
		<?= GridView::widget([
		'dataProvider' => $provider,
		'columns' => [
                    [
                        'attribute' => 'time_performed',
                        'label' => Yii::t('app', 'Time Performed'),
                    ],
                    [
                        'attribute' => 'time_expected_start',
                        'label' => 'Запланировано',
					],
                    [
                        'attribute' => 'time_expected_end',
                        'label' => 'Завершение',
					],
                    [
                        'attribute' => 'type',
                        'label' => Yii::t('app', 'Type'),
						'value' => function ($model){
							return @\backend\models\CallType::find()->where(['id'=>$model['type']])->one()['name'];
						},
                    ],
                    [
                        'attribute' => 'address',
                        'label' => Yii::t('app', 'Address'),
                    ],
                    [
                        'attribute' => 'phone',
                        'label' => Yii::t('app', 'Phone'),
                    ],
                    [
                        'attribute' => 'contact',
                        'label' => Yii::t('app', 'Contact'),
                    ],
                    [
                        'attribute' => 'comment',
                        'label' => Yii::t('app', 'Comment'),
                    ],
                    /*[
                        'attribute' => 'user_name',
                        'label' => Yii::t('app', 'User Name r'),
                    ],/**/
		],
	])/**/?>
		</div>
	</div>
<?}?>
</div>
