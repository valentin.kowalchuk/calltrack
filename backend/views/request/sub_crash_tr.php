<?php

/** @var yii\data\ActiveDataProvider $dataProvider */

/** @var backend\controllers\RequestSearch $searchModel */

use backend\models\CallType;
use backend\models\User;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$only_rec = !empty($only_rec);

$aa = [];
if ($only_rec) {
    $aa = [
        'layout' => '{items}',
        'showHeader' => false,
    ];
}

$columns = [
    //['class' => 'yii\grid\SerialColumn'],
    //['class' => 'yii\grid\CheckboxColumn'],
    [
        'class' => 'app\widgets\ExColumn',
        'type' => 'btn',
        'value' => function ($model) {
            if ($model->crash_id !== null)
                return '<span style="color:#C88; font-size:22px; vertical-align:midle;" class="glyphicon glyphicon-exclamation-sign text-center"></span>';
            else
                return '';
        },
        'format' => 'raw',
        'contentOptions' => function ($model, $key, $index, $cell) {
            $class = 'col_al_center';
            return ['class' => $class];
        },
    ],
    [
        'attribute' => 'id',
        'class' => 'app\widgets\ExColumn',
        'contentOptions' => function ($model, $key, $index, $cell) {
            $class = 'id_col color_rec_' . $model->type;
            if ($model->type == 1) $class .= ' cel_crash';
            return [
                'class' => $class
            ];
            return ['class' => $class];
        },
        'filterInputOptions' => ['class' => 'form-control id_src_inp'],
    ],
    [
        'class' => 'app\widgets\ExColumn',
        'type' => 'sel_radio',
        'attribute' => 'type',
        'filter' => ArrayHelper::map(CallType::find()->all(), 'id', 'name'),
        'filterInputOptions' => [
            'class' => 'form-control-ym',
            'id' => null
        ],
        'value' => function ($model) {
            return implode($model->getTypesNames(), ', ');
        },
    ],
    [
        'attribute' => 'time_add',
        'attribute_min' => 'time_add_min',
        'attribute_max' => 'time_add_max',

        'class' => 'app\widgets\ExColumn',
        'filter' => true,

        'type' => 'range',
        'value' => function ($data) {
            //return date("Y-m-d H", strtotime($data->time_add));
            return $data->time_add;
        },
    ],
    [
        'attribute' => 'time_expected_start',

        'attribute_min' => 'time_expected_start_min',
        'attribute_max' => 'time_expected_start_max',

        'class' => 'app\widgets\ExColumn',
        'filter' => true,
        'type' => 'range',

        'value' => function ($data) {
            $a_str = [$data->time_expected_start];
            if (!empty($data->time_expected_hh_end)) {
                $a_str[] = '&ndash;';
                $a_str[] = $data->time_expected_hh_end;
            }
            return implode($a_str, '');
        },
        'format' => 'raw',
    ],
    [
        'attribute' => 'time_performed',
        'attribute_min' => 'time_performed_min',
        'attribute_max' => 'time_performed_max',
        'class' => 'app\widgets\ExColumn',
        'filter' => true,
        'type' => 'range',
        'value' => function ($data) {
            if ($data->time_performed)
                return date("Y-m-d H", strtotime($data->time_performed));
            else
                return null;
        },
    ],
    [
        'attribute' => 'address',
        'value' => function ($data) {
            return /*$data->address_id. ' | ' ./**/ $data->address;
        }
    ],
    [
        'attribute' => 'phone',
        'value' => function ($data) {
            return implode([$data->phone, $data->phone1], ' ');
        }
    ],
    [
        'attribute' => 'contact',
        'value' => function ($data) {
            return implode([$data->contact, $data->contact1], ' ');
        }
    ],
    [
        'attribute' => 'comment',
        'value' => function ($data) {
            return iconv_substr($data->comment, 0, 20, 'UTF8');
        },
    ],
    //'comment:ntext',
    [
        'attribute' => 'responsible_user',
        'filter' => ArrayHelper::map(User::find()->all(), 'id', 'userfullname'),
        'filterInputOptions' => [
            'class' => 'form-control-ym',
            'id' => null
        ],
        'value' => function ($data) {
            $responsible_user_info = ArrayHelper::map($data->responsible_user_info, 'id', 'userfullname');
            return implode((array)$responsible_user_info, ', ');
        },
        'format' => 'raw',
    ],
    [
        'class' => 'app\widgets\ActionColumn',
        'type' => 'hbtn',
        'template' => '<p style="white-space: nowrap;">{update} {view} {done}</p>',
        'buttons' => [
            'update' => function ($url, $model) {
                if (!Yii::$app->user->can('request_edit')) return;
                $url = Url::toRoute(['/request/update', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                    'title' => 'Редактирование',
                    'data-pjax' => '0',            // отключение стандартного обработчика pjax. Он все портит:)
                    'class' => 'j_edit_request'    // ссылке класс:)
                ]);
            },
            'view' => function ($url, $model) {
                if (!Yii::$app->user->can('request_view')) return;
                $url = Url::toRoute(['/request/view', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                    'title' => 'Просмотр',
                    'data-pjax' => '0',
                    'class' => 'j_view_request'
                ]);
            },
            'done' => function ($url, $model) {
                if (!Yii::$app->user->can('request_done')) return;
                $url = Url::toRoute(['/request/done', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, [
                    'title' => 'Закрытие',
                    'data-pjax' => '0',
                    'class' => 'j_done_request'
                ]);
            },
        ],

    ]
];
echo '<div class="pull-right">';
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $columns,
    'pjax' => false,
    'dropdownOptions' => [
        'label' => 'Export All',
        'class' => 'btn btn-outline-secondary'
    ],
    'exportConfig' => [
        ExportMenu::FORMAT_HTML => false,
        ExportMenu::FORMAT_EXCEL => false,
        ExportMenu::FORMAT_TEXT => false,
        ExportMenu::FORMAT_CSV => [
            'label' => Yii::t('app', 'CSV'),
        ],
        ExportMenu::FORMAT_EXCEL_X => false,
    ],
]);
echo '</div>';
echo '<br>';

echo GridView::widget($aa + [
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax' => false,
        'pjaxSettings' => [
            'options' => [
                'id' => 'requests'
            ]
        ],
        'rowOptions' => function ($model, $key, $index, $grid) {
            if ($model->time_expected_end) {

            }
            $class = 'color_rec_' . $model->type;
            if ($model->type == 1) $class .= ' rec_crash';
            if (((strtotime($model->time_expected_start . ':00:00') - time()) / 60 / 60 < 1)
                and (strtotime($model->time_expected_end . ':00:00') - time() > 0)
                and ($model->time_performed == null))
                $class .= ' caution';
            if (!empty($model->time_expected_end))
                if (((strtotime($model->time_expected_end . ':00:00') - time() < 0) and ($model->time_performed == null))
                    or (strtotime($model->time_expected_end . ':00:00') - strtotime($model->time_performed) < 0))
                    $class .= ' overdue';

            return [
                'class' => $class,
            ];
        },
        'columns' => $columns
    ]
);
