<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/* @var $this yii\web\View */
/* @var $model app\models\CallRequest */

$this->title = Yii::t('app', 'Call Request') . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Call Requests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modal-body">
    <button type="button" class="close" data-dismiss="modal">
        <span aria-hidden="true">&times;</span><span class="sr-only"><?Yii::t('app', 'Close')?></span>
    </button>
<div class="call-request-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
				'attribute' => 'time_add',
				'title' => 'dd',
			],
            'time_performed',
            [
				'attribute' => 'type',
				'value' => @\backend\models\CallType::find()->where(['id'=>$model->type])->one()['name'],
			],
			'time_expected_start',
			'time_expected_end',
            'address',
            /*[
				'phone' => 'type',
				'value' => @\backend\models\CallType::find()->where(['id'=>$model->type])->one()['name'],
			],/**/
            'phone',
            'contact',
            'phone1',
            'contact1',
            'comment:ntext',
            [
				'label' => 'Комментарий при выполнении',
				'attribute' => 'comment_done',
				'format' => 'ntext',
			],
        ],
    ]) ?>
</div>


<?if(Yii::$app->user->can('request_view_history')){?>
	<?php $temp = \app\models\CallRequest::getLog($model->id); ?>
	<? if(!empty($temp)){?>
    <div class="call-request-view">
    <h1><?=Yii::t('app', 'History corrections')?></h1>

 <?php  /**/$provider = new ArrayDataProvider([
            'allModels' => $temp,
            ]);
             // $provider->pagination = ['defaultPageSize' => 100];
                $provider->pagination = false;
                       /**/  ?>
     <div class="my-history">
    <?= GridView::widget([
		'dataProvider' => $provider,
		'columns' => [
                    [
                        'attribute' => 'time',
                        'label' => Yii::t('app', 'Time Modified'),
                    ],
                    [
                        'attribute' => 'time_expected_end',
                        'label' => 'Завершение',
					],
                    [
                        'attribute' => 'type',
                        'label' => Yii::t('app', 'Type'),
						'value' => function ($model){
							return @\backend\models\CallType::find()->where(['id'=>$model['type']])->one()['name'];
						},
                    ],
                    [
                        'attribute' => 'address',
                        'label' => Yii::t('app', 'Address'),
                    ],
                    [
                        'attribute' => 'contact',
                        'label' => Yii::t('app', 'Contact'),
						'value' => function ($model){
							return $model['phone'] . (!empty($model['contact']) ? ', ' . $model['contact'] : '');
						},
                    ],
                    [
                        'attribute' => 'contact1',
                        'label' => Yii::t('app', 'Contact'),
						'value' => function ($model){
							return $model['phone1'] . (!empty($model['contact1']) ? ', ' . $model['contact1'] : '');
						},
                    ],
                    [
                        'attribute' => 'comment',
                        'label' => Yii::t('app', 'Comment'),
                    ],
                    [
                        'attribute' => 'user_name',
                        'label' => Yii::t('app', 'User Name r'),
                    ],
		],
	])?>
     </div>
    </div>
	<?}?>
<?}?>
</div>
