$(document).ready(function () {
	//{ address modal selects
	$('#address-street, #address-house, #address-city').change(function () {
		var sel = $(this).closest(".j_col_addr").next().find(".form-control");
		var id = $(this).val();
		console.log(id);
		if(parseInt(id)>0)
		{
			var data = {'id':id};
			var url = $('#j_path_addr_inp').val();						
			$.ajax({
				url: url,
				type: "GET",
				data: data,
				success: function (data){
					var html = $(data).html();
					console.log(html);
					$(sel).html(html);
					$(sel).change();
				}
			});
		}
		else
		{
			$(sel).html('');
			$(sel).change();
		}

		$('#address-house').change(function () {
			var temp = $(this).val();
			$('#address-dependency_id').val(temp);
		});
	});
	//}				
});