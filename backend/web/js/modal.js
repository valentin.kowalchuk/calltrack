$(document).ready(function () {
	var modalContainer = $('#modal-req');
	var modalСontent = modalContainer.find('.modal-content');
	var modalBody = modalContainer.find('.modal-body');
	var modalMsg = $(modalContainer).find(".j_msg");
	var modalFooter = $(modalContainer).find(".modal-footer");

	var modalCheckCrashContainer = $('#modal-check-crash');
	var modalCheckCrashBody = modalCheckCrashContainer.find('.modal-content');
	function visibility_blocks() {	
		var type = $('#callrequest-type').val();		
		switch (type){
			case '2':
				$('.modal-body .row.j_address').hide();
			break;
			case '15':
				$('.modal-body .row.j_address').hide();
			break;
			default:
				$('.modal-body .row').show();								
		}
	};
	visibility_blocks();
	
	function navigate(t) {
		var sel_inp_s = 'input[type="text"], input[type="radio"], select:visible, textarea, button';
		sel_inp = $(t).find(sel_inp_s).filter(":visible");
		$(sel_inp).eq(1).focus();
		$(sel_inp).unbind('keydown');
		$(sel_inp).keydown(function( event ){
			var index = $(sel_inp).index(this);
			if (index<0)
				return false;
			var inc=0;
			if ( event.which == '39' )
				inc=1;
			else
			if ( event.which == '37' )
				inc=-1;
			if(inc!=0){
				var n=50;
				while ((n>0)&&(index>=0)){
					n--;
					index+=inc;
					var el = $(sel_inp).eq(index);
					if(($(sel_inp).index(el)>=0)&&($(sel_inp).index(el)<$(sel_inp).length))
					{
						$(el).focus();
						$(el).select();
						return false;
						break;
					}
					else
						$(sel_inp).eq(1).focus();
				}
			}			
		});
	}
	
	function bind_request_act(){
		$('.disabled_form').find('.form-control, input').attr('readonly', 'readonly').css('cursor', 'default');
		$('.disabled_form').find('input[type="checkbox"]').attr('disabled', 'disabled');
		$('.disabled_form').find('select, #j_check').attr('disabled', 'disabled');
		
		$('.j_datetimepicker').datetimepicker({
			format: 'YYYY-MM-DD HH'
		});
		$('.j_datetimepicker_h').datetimepicker({
			format: 'YYYY-MM-DD'
		});
		
		$('#date_expected_d, #time_expected_hh_start, #time_expected_hh_end, .btn').unbind('change keyup blur');
		$('#date_expected_d, #time_expected_hh_start, #time_expected_hh_end, .btn').bind('change keyup blur',function (){
			if($('#date_expected_d').val() !== "undefined" && $('#date_expected_d').val().length==10) {
				var v1 = $('#time_expected_hh_start').val(); if (v1.length<2) v1 = '0'+v1;
				v1 = v1.substr(0, 2);
				$('#callrequest-time_expected_start').val($('#date_expected_d').val()+' '+v1);
				var v2 = $('#time_expected_hh_end').val();
				v2 = v2.substr(0, 2);
				if (v2.length>0)
				{
					if (v2.length<2) v2 = '0'+v2;
					$('#callrequest-time_expected_end').val($('#date_expected_d').val()+' '+v2);
				}
				else
					$('#callrequest-time_expected_end').val('');
			}
			if($('#date_expected_d').val() !== "undefined" && $('#date_expected_d').val().length==0) {
				$('#callrequest-time_expected_start').val('');
				$('#callrequest-time_expected_end').val('');
			}			
		});
		
		//{ проверка адреса
		$('#j_check').unbind('click');
		$('#j_check').click(function () {
				var id = $('#j_model_id').val();
				var id = $('#callrequest-house').val();							
				var data = {'id':id};
				var url = $('#j_view_posible_dependency_inp').val();
				$.ajax({
					url: url,
					type: "GET",
					cache: false,
					data: data,
					success: function (data){
						$(modalCheckCrashBody).html(data);
						modalCheckCrashContainer.modal();
						navigate(modalCheckCrashContainer);
					}
				});							
		});
		//} проверка адреса
					
		$('#j_create_request, .j_edit_request, .j_view_request, .j_done_request').unbind('click');
		$('#j_create_request, .j_edit_request, .j_view_request, .j_done_request').click(function(event){ // нажатие на кнопку - выпадает модальное окно
			event.preventDefault();
			var url = $(this).attr('href');
			var clickedbtn = $(this);
			$.ajax({
				url: url,
				type: "GET",
				data: {/*'userid':UserID*/},
				cache: false,
				success: function (data) {
					//$('.modal-content').html(data);
					$(modalСontent).html(data);
					visibility_blocks();
					modalContainer.modal({show:true});
					navigate(modalContainer);
					bind_request_act();
				}
			});		
			return false;
		});
		
		//{
		$('#callrequest-type').unbind('change');
		$('#callrequest-type').change(function () {
			visibility_blocks();
		});
		//}
		
		//{ address modal selects
		$('#callrequest-street, #callrequest-house, #callrequest-city').unbind('change');
		$('#callrequest-street, #callrequest-house, #callrequest-city').change(function () {
			var sel = $(this).closest(".j_col_addr").next().find(".form-control");
			var sel_modal_dialog = $(this).closest(".j_col_addr").next().find(".modal-dialog");
			var inp_sel = $(this).closest(".j_col_addr").next().find(".inp_sel");
			
			var id = $(this).val();
			if(parseInt(id)>0)
			{
				var data = {'id':id};
				var url = $('#j_path_addr_inp').val();						
				$.ajax({
					url: url,
					type: "GET",
					cache: false,
					data: data,
					success: function (data){
						var html = $(data).html();
						$(sel).html(html);
						$(sel).change();
						
						$(sel_modal_dialog).html($(sel).html());
						$(sel_modal_dialog).find("option").each(function( index ) {
							$(this).val($(this).text());
						});
						$(sel_modal_dialog).change();
						
						$(inp_sel).val($(sel_modal_dialog).val());
						$(inp_sel).change();
					}
				});
			}
			else
			{
				$(sel).html('');
				$(sel).change();
			}
		});
		
		
		$('.inp_sel, #state_list>option').unbind("change");
		$('.inp_sel, #state_list>option').bind("change", function () {//keyup //blur //click
			var sel_tag = $(this).closest('.j_col_addr').find('select.form-control');
			var text = $(this).val();
			var option_val = $(sel_tag).find("option").filter(function(){
				return $(this).text()==text;
			}).val();
			$(sel_tag).val(option_val);
			$(sel_tag).change();
		});/**/
		//} address modal selects
	}
	bind_request_act();
	
	//{?
    $(document).on("click", '#j_check', function (e) {
		var sel = $('.request_form').find('.form-control').eq(0);
		return false;
    });
	//}
	
	//{ 
    $(document).on("submit", '.request_form, .request_done_form', function (e) {
        e.preventDefault();
        var form = $(this);
		var url = $(form).attr('action');
        $.ajax({
			url: url,
			type: "POST",
			data: form.serialize(),
			dataType: 'json',
			cache: false,
			success: function (data){
				var modalContainer = $('#modal-req');
				var modalСontent = modalContainer.find('.modal-content');
				var modalBody = modalContainer.find('.modal-body');
				var modalMsg = $(modalContainer).find(".j_msg");
				var modalFooter = $(modalContainer).find(".modal-footer");
				/**/
				
				var result = null;
				
				if (typeof(data) == 'object')
					result = data.result;
				
				if (result == true)
				{
					modalMsg.html('');
					modalBody.html('');
					modalFooter.html('');
					setTimeout(function() {
							$("#modal-req").modal('hide');
							document.location = document.location;
						}, 1000
					);
					/**/
				}
				else {
					var content = data.content;
					modalСontent.html(content);
					modalСontent.hide().fadeIn();
					navigate(modalContainer);
				}
				//--------------------------------------------------------------------------------
				var modalMsg = $(modalContainer).find(".j_msg");
				
				if (typeof(data.msg) == 'string')
				{
					modalMsg.html('');
					var msg = data.msg;
					var new_msg = $("<div>").addClass("alert alert-success");
					$(new_msg).append("<strong>"+msg+"</strong>");
					$(new_msg).appendTo(modalMsg);
				}
				
				bind_request_act();
			}
		});
	});
	//}

	$(modalContainer).on('shown.bs.modal', function () {
		$(modalContainer).find('.form-control:visible').eq(0).focus();
		navigate(modalContainer);
	});

	$(modalCheckCrashContainer).on('shown.bs.modal', function () {
		navigate(modalCheckCrashContainer);
		$(modalCheckCrashBody).find('input[type="radio"]')[0].focus();
	});
	
	$(modalCheckCrashContainer).on('hidden.bs.modal', function () {
		navigate(modalContainer);
		$('#j_check').focus();
	});
	
	//{
	$(document).on("submit", '.check_address_form', function (e) { //, .request_done_form
		var dependency_address_id = $('input[name=dependency_address_id]:checked').val();
		$("#callrequest-crash_id").val(dependency_address_id);
		if((dependency_address_id>''))
		{
			$(".j_trash_info_id").html(dependency_address_id);
			$(".j_trash").removeClass('hide');
			$(".j_trash_info_address").html('');
			$(".j_trash_info_comment").html('');	
		}
		else
		{
			$(".j_trash").addClass('hide');
		}
		modalCheckCrashContainer.modal('hide');
		
		modalContainer.modal();
		navigate(modalContainer);
		return false;
	});
	//}
	
	//{
	$('.j_show_child_requests').click(function (){
		var rec_crash = $(this).closest('.rec_crash').next();
		if ($(rec_crash).hasClass('hidden'))
		{
			$(rec_crash).removeClass('hidden');
			var id = $(this).attr('r_id');
			var data = {id:id};
			var url = $('#j_view_get_sub_crash').val();
			$.ajax({
				url: url,
				type: "GET",
				cache: false,
				data: data,
				success: function (data){
					$(rec_crash).after($(data).find('tbody').children().addClass('j_sub_crash'));
					bind_request_act();
				}
			});
		}
		else
		{
			$('.j_sub_crash').remove();
			$(rec_crash).addClass('hidden');
		}
	});
	//}
});