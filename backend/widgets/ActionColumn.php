<?php

namespace app\widgets;

use yii\helpers\Html;

class ActionColumn extends \yii\grid\ActionColumn
{
    public $type = null;

    protected function renderFilterCellContent()
    {
        switch ($this->type) {
            case 'hbtn':
                return Html::Button('Искать', ['type' => 'submit', 'class' => 'j_sb_src btn btn-xs btn-warning']) . '<br/>'
                    . Html::a('Очистить', ['index'], ['class' => 'btn btn-xs btn-info']);

            default:
                return parent::renderFilterCellContent();
        }
    }
}