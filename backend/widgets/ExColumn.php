<?php

namespace app\widgets;

use yii\grid\DataColumn;
use yii\helpers\Html;

class ExColumn extends DataColumn
{
    public $type = null;
    public $attribute_min = null;
    public $attribute_max = null;
	
    protected function renderFilterCellContent()
    {
        if (is_string($this->filter)) {
            return $this->filter;
        }

        $model = $this->grid->filterModel;
		switch($this->type){
			case 'range':
				$error = '';
				foreach (['attribute_min', 'attribute_max'] as $attr)
				{
					if ($model->hasErrors($this->$attr)) {
						Html::addCssClass($this->filterOptions, 'has-error');
						$error .= ' ' . Html::error($model, $this->$attr, $this->grid->filterErrorOptions);
					}
				}
				

				$s = '';
				$s .= \Yii::$app->controller->renderPartial('inp_datetimepicker', ['m'=>Html::activeTextInput($model, $this->attribute_min, $this->filterInputOptions)]);
				$s .= \Yii::$app->controller->renderPartial('inp_datetimepicker', ['m'=>Html::activeTextInput($model, $this->attribute_max, $this->filterInputOptions)]);
				$s .= $error;
				return $s;
			case 'sel_radio':
				$s = parent::renderFilterCellContent();
				$s .= Html::activeRadioList($model,'performed', ['1'=>'откр.','2'=>'закр.','0'=>'все']);
				return $s;
			case 'btn':
			default:
				return parent::renderFilterCellContent();
		}
    }
	
    protected function renderDataCellContent($model, $key, $index)
    {
		$result = parent::renderDataCellContent($model, $key, $index);
		switch($this->type){
		case 'btn':
			if($model->type == 1)
			{
				$result .= '<button class="j_show_child_requests btn btn-default btn-xs" r_id="'.$model->id.'"><span class="glyphicon glyphicon-sort-by-attributes"></span></button>';
			}
		default:
		}	
		return $result;
	}
}