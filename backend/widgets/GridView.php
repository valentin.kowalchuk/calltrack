<?php

namespace app\widgets;

use Yii;
use yii\grid\DataColumn;
use yii\helpers\Html;

class GridView extends \yii\grid\GridView
{
    public $message;

    public function init()
    {
        parent::init();
    }


    public function renderTableRow($model, $key, $index)
    {
		$result = parent::renderTableRow($model, $key, $index);
		if($model->type == 1){
			$td = Html::tag('td', '', ['colspan'=>12]);
			$result .= Html::tag('tr', $td, ['class'=>'hidden']);
		}
		return $result;
	}

    public function renderItems()
    {
        $caption = $this->renderCaption();
        $columnGroup = $this->renderColumnGroup();
        $tableHeader = $this->showHeader ? $this->renderTableHeader() : false;
        $tableBody = $this->renderTableBody();
        $tableFooter = $this->showFooter ? $this->renderTableFooter() : false;
        $content = array_filter([
            $caption,
            $columnGroup,
            $tableHeader,
            $tableFooter,
            $tableBody,
        ]);

        return Html::tag('table', implode("\n", $content), $this->tableOptions);
    }
	
    protected function createDataColumn($text)
    {
        if (!preg_match('/^([^:]+)(:(\w*))?(:(.*))?$/', $text, $matches)) {
            throw new InvalidConfigException('The column must be specified in the format of "attribute", "attribute:format" or "attribute:format:label"');
        }

        return Yii::createObject([
            'class' => $this->dataColumnClass ? : DataColumn::className(),
            'grid' => $this,
            'attribute' => $matches[1],
            'format' => isset($matches[3]) ? $matches[3] : 'text',
            'label' => isset($matches[5]) ? $matches[5] : null,
        ]);
    }
	
}