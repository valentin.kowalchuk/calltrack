<?php
return [
    'adminEmail' => '',
    'supportEmail' => '',
    'user.passwordResetTokenExpire' => 3600,
    'bsVersion' => '3.x',
//    'bsDependencyEnabled' => false
];
